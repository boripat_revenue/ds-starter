program DSStarter;

uses
  Forms,
  fmStarter in 'fmStarter.pas' {frmStarter};

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'InfoExpress Startup';
  Application.CreateForm(TfrmStarter, frmStarter);
  Application.Run;
end.
