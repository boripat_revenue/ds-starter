unit fmStarter;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,   IdHTTP, IdBaseComponent, IdComponent, IdTCPConnection,ShellApi,
  IdTCPClient, ExtCtrls, ComCtrls, INIFILES,NB30, StdCtrls,IdHashMessageDigest,idHash,strUtils;

type
  TfrmStarter = class(TForm)
    idHTTP: TIdHTTP;
    barDownload: TProgressBar;
    barFile: TProgressBar;
    txtCode: TEdit;
    txtMacAddr: TEdit;
    txtComID: TEdit;
    mmoResult: TMemo;
    mmoAction: TMemo;
    idDownload: TIdHTTP;
    timStart: TTimer;
    procedure FormCreate(Sender: TObject);
    procedure idDownloadWorkBegin(Sender: TObject; AWorkMode: TWorkMode;
      const AWorkCountMax: Integer);
    procedure idDownloadWork(Sender: TObject; AWorkMode: TWorkMode;
      const AWorkCount: Integer);
    procedure idDownloadWorkEnd(Sender: TObject; AWorkMode: TWorkMode);
    procedure timStartTimer(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  function ExtractResponse(Input: string; slice:char): TStringList;
  function GetRegistry(sType:string):string;
  function AddRegistry(sKey,sValue:string):boolean;
  function GetServerUpdate():string;
  procedure GetHTTPData(sURL:string);
  procedure GetUpdate();
  function DownloadFile(fname,fpath,floc,fhash:string;fsize:integer;idDown:TidHTTP):boolean;
  function CheckDownloadUpdate():boolean;
  procedure ReadConfig();
  end;

const
  cProduct='InfoExpress-Lite';
  cKeyMonitor='Monitor';
  cKeyPlayer='Player';
  cKeyAgent='Agent';
  cKeyVersion='Version';
  cKeyCompID='ComputerID';
  cKeyExpire='Expire';
  cSlice='|';
var
  frmStarter: TfrmStarter;
  slOutput: TStringList;  // Keep Extract Data From HTTP Format
  slData: TStringList;  // Keep Extract Data From HTTP Format
  memDownload: TMemoryStream; // Keep Data from HTTP
  bServUpdate:boolean; // Keep Update Status from Server
  sCurrVersion: string; // Keep Current Version from This Computer
  sServVersion: string; // Keep Last Version from Server
  sServURL: string; // Keep URL for Download

  sProgramDir: string; // Keep Main Application Directory
  sURLServer: string; // Keep Server URL

  procedure dll_GetSerialize(out strOut: pChar); external 'RevenueExpress.dll';
  procedure dll_GenMD5(const strHash: pChar; Out strOut: pChar);  external 'RevenueExpress.dll';
  procedure dll_GetRegistry(const strPName: PChar; const strKey: PChar;Out strOut: PChar); external 'RevenueExpress.dll';
  function dll_AddRegistry(const strPName: PChar;const strKey: pChar; const strValue: pChar): boolean; external 'RevenueExpress.dll';
  procedure FreePChar(p: Pchar); external 'RevenueExpress.dll';

implementation

{$R *.dfm}

function getMD5checksum(const fileName : string) : string;
var
  idmd5 : TIdHashMessageDigest5;
  fs : TFileStream;
//  hash : T4x4LongWordRecord;
begin
  idmd5 := TIdHashMessageDigest5.Create;
  fs := TFileStream.Create(fileName, fmOpenRead OR fmShareDenyWrite) ;
  try
    result := idmd5.AsHex(idmd5.HashValue(fs)) ;
  finally
    fs.Free;
    idmd5.Free;
  end;
end;

function GetFileSize(fileName : wideString) : Int64;
var
   sr : TSearchRec;
begin
  if FindFirst(fileName, faAnyFile, sr ) = 0 then
    result := Int64(sr.FindData.nFileSizeHigh) shl Int64(32) + Int64(sr.FindData.nFileSizeLow)
  else
    result := -1;
  FindClose(sr);
end;


function GetAdapterInfo(Lana: Char): String;
var
  Adapter: TAdapterStatus;
  NCB: TNCB;
begin
  FillChar(NCB, SizeOf(NCB), 0);
  NCB.ncb_command := Char(NCBRESET);
  NCB.ncb_lana_num := Lana;
  if Netbios(@NCB) <> Char(NRC_GOODRET) then
  begin
    Result := 'No MAC';
    Exit;
  end;

  FillChar(NCB, SizeOf(NCB), 0);
  NCB.ncb_command := Char(NCBASTAT);
  NCB.ncb_lana_num := Lana;
  NCB.ncb_callname := '*';

  FillChar(Adapter, SizeOf(Adapter), 0);
  NCB.ncb_buffer := @Adapter;
  NCB.ncb_length := SizeOf(Adapter);

  if Netbios(@NCB) <> Char(NRC_GOODRET) then
  begin
    Result := 'No MAC';
    Exit;
  end;

  Result :=
    IntToHex(Byte(Adapter.adapter_address[0]), 2) + ':' +
    IntToHex(Byte(Adapter.adapter_address[1]), 2) + ':' +
    IntToHex(Byte(Adapter.adapter_address[2]), 2) + ':' +
    IntToHex(Byte(Adapter.adapter_address[3]), 2) + ':' +
    IntToHex(Byte(Adapter.adapter_address[4]), 2) + ':' +
    IntToHex(Byte(Adapter.adapter_address[5]), 2);
end;

function GetMACAddress: string;
var
  AdapterList: TLanaEnum;
  NCB: TNCB;
begin
  FillChar(NCB, SizeOf(NCB), 0);
  NCB.ncb_command := Char(NCBENUM);
  NCB.ncb_buffer := @AdapterList;
  NCB.ncb_length := SizeOf(AdapterList);
  Netbios(@NCB);

  if Byte(AdapterList.length) > 0 then
    Result := GetAdapterInfo(AdapterList.lana[0])
  else
    Result := 'No MAC';
end;


function TfrmStarter.ExtractResponse(Input: string; slice:char): TStringList;
var
  i: integer;
  Temp: string;

begin
  slOutput.Clear;
  Temp := '';

  for i := 1 to Length(Input) do
  begin
    if (Input[i] <> slice) and (i <> Length(Input)) then
    begin
      Temp := Temp + Input[i];
    end
    else
    begin
      if (Trim(Input[i]) <> '') and (Input[i] <> slice) then
        Temp := Temp + Input[i];
      slOutput.Add(Trim(Temp));
      Temp := '';
    end;
  end;

  result := slOutput;
end;

function TfrmStarter.GetRegistry(sType:string):string;
var
  pData: pChar;
  sTmp: string;
begin
  dll_getRegistry(cProduct,pchar(sType),pData);
  sTmp:=pData;
  FreePChar(pData);
  result:=sTmp;
end;

function TfrmStarter.AddRegistry(sKey,sValue:string):boolean;
begin
  result:=dll_AddRegistry(cProduct,pchar(sKey),pchar(sValue));
end;

procedure TfrmStarter.FormCreate(Sender: TObject);
begin
  slOutput := TStringList.Create;
  slData := TStringList.Create;
  memDownload := TMemoryStream.Create;
  sProgramDir:=ExtractFilePath(Application.ExeName);
  txtMacAddr.Text:=GetMACAddress;
  mmoAction.Clear;
  mmoResult.Clear;
  ReadConfig();
  if (not DirectoryExists(sProgramDir+'/tmp/')) then
    CreateDir(sProgramDir+'/tmp/');

end;

procedure TfrmStarter.GetUpdate();
var
  sURL:string;
  iTotalFile:integer;
  iRun:integer;
  sName,sPath,sLoc,sHash:string;
  iSize:INT64;
begin
//  sURL:=sURLServer+'?cmd=get_download&attr=version&value='+sServVersion;
  sURL:=sServURL;
  GetHTTPData(sURL);

  iTotalFile:=mmoResult.Lines.Count-1;
  barFile.Max:=iTotalFile;
  barFile.Min:=0;
  for iRun:=1 to iTotalFile do begin
    slData:=ExtractResponse(mmoResult.Lines[iRun],cSlice);
    // filename[0],path[1],url[2],filesize[3],hash[4]
    sName:=slData.Strings[0];
    sPath:=slData.Strings[1];
    sLoc:=slData.Strings[2];
    iSize:=strtoint(slData.Strings[3]);
    sHash:=slData.Strings[4];
    mmoAction.Lines.Add('Start Download "'+sName+'"');
    if (DownloadFile(sName,sPath,sLoc,sHash,iSize,idDownload)) then begin
      mmoAction.Lines.Add('Download "'+sName+'" Completed.')
    end else begin
      mmoAction.Lines.Add('Download "'+sName+'" Failed.')
    end;
    barFile.Position:=iRun;
    sleep(1000);
  end;
  mmoAction.Lines.Add('Download New Version Completed.')
end;

function TfrmStarter.GetServerUpdate;
var
  sURL:string;
begin
  sURL:=sURLServer+'?cmd=check_version';
  GetHTTPData(sURL);

  slData:=ExtractResponse(mmoResult.Lines[1],cSlice);
  // Version,boolUpdate,dateUpdate,urlUpdate
  sServVersion:=slData.Strings[0];
  bServUpdate:=(lowercase(slData.Strings[1])= 'y');
  sServURL:=slData.Strings[3];
end;

procedure TfrmStarter.GetHTTPData(sURL:string);
var
  sHTTP:String;
begin
  memDownload.Clear;
  sHTTP:=sURL+'&mc='+txtComID.Text;
  IdHTTP.get(sHTTP, memDownload);
  Application.ProcessMessages;

  memDownload.Position := 0;
  mmoResult.Clear;
  mmoResult.Lines.LoadFromStream(memDownload);

  memDownload.Clear;
end;

function TfrmStarter.CheckDownloadUpdate:boolean;
begin
  result:=FALSE;
  getServerUpdate();
  if (bServUpdate) then begin
    if (sServVersion > sCurrVersion) then begin
      result:=TRUE;
    end;
  end;
end;

procedure TfrmStarter.ReadConfig();
var iniFile: TINIFile;
begin
  bServUpdate:=false;
  sCurrVersion:=GetRegistry(cKeyVersion);
  txtComID.Text:=GetRegistry(cKeyCompID);
  txtCode.Text:=AnsiLeftStr(txtComID.Text,4)
    + '-'+ AnsiRightStr(txtComID.Text,4);
  sURLServer:='http://online.fdsthai.com/server.php';
  iniFile:=TiniFile.Create(sProgramDir+'config.ini');
  try
    sURLServer:=iniFile.ReadString('server','update','http://online.fdsthai.com/server.php');
  finally
    iniFile.Free;
  end;
end;

function TfrmStarter.DownloadFile(fname,fpath,floc,fhash:string;fsize:integer;idDown:TidHTTP):boolean;
var sRequest,sHash,sFile,sTemp: string;
begin
  result:=false;
  try
    memDownload.Clear;
    sRequest:=floc+'?mc='+txtComID.Text;
    idDown.get(sRequest, memDownload);
//    Application.ProcessMessages;


    memDownload.Position := 0;
    sTemp:=format('%s\tmp\%s',[sProgramDir,fname]);

    sFile:=format('%s\%s\%s',[sProgramDir,fpath,fname]);
    if (length(fpath)=0) then begin
      sFile:=format('%s\%s',[sProgramDir,fname]);
    end else begin
      if (not DirectoryExists(sProgramDir+fpath)) then
        CreateDir(sProgramDir+fpath);
    end;

    if (FileExists(sTemp)) then
      DeleteFile(sTemp);
    memDownload.SaveToFile(sTemp);
    sHash:=getMD5CheckSum(sTemp);
    if (fHash = sHash) then begin
      if (FileExists(sFile)) then
        DeleteFile(sFile);
      MoveFile(pchar(sTemp),pchar(sFile));
      result:=true;
    end;
  except
  end;
  memDownload.Clear;
end;


procedure TfrmStarter.idDownloadWorkBegin(Sender: TObject;
  AWorkMode: TWorkMode; const AWorkCountMax: Integer);
begin
  with barDownload do begin
    Min:=0;
    Max:=AWorkCountMax;
    Position:=0;
  end;
end;

procedure TfrmStarter.idDownloadWork(Sender: TObject; AWorkMode: TWorkMode;
  const AWorkCount: Integer);
begin
  barDownload.Position:=AWorkCount;
end;

procedure TfrmStarter.idDownloadWorkEnd(Sender: TObject;
  AWorkMode: TWorkMode);
begin
  barDownload.Position:=0;
end;

procedure TfrmStarter.timStartTimer(Sender: TObject);
begin
  timStart.Enabled:=false;
  mmoAction.Lines.Add('Checking New Version');
  sleep(1000);
  if (CheckDownloadUpdate) then begin
    mmoAction.Lines.Add('Found New Version');
    getUpdate();
    addRegistry(cKeyVersion,sServVersion);
  end else begin
    mmoAction.Lines.Add('No New Version');
  end;
  Sleep(2000);
  ShellExecute(Handle, 'open', pchar(sProgramDir+'dsmon.exe'), nil, nil, SW_SHOWNORMAL);
  Application.Terminate;
end;

procedure TfrmStarter.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  slOutput.Free;
  slData.Free;
  memDownload.Free;
end;

procedure TfrmStarter.FormShow(Sender: TObject);
begin
  frmStarter.Left:=screen.WorkAreaWidth-frmStarter.Width;
  frmStarter.Top:=screen.WorkAreaHeight-frmStarter.Height;
end;

end.
